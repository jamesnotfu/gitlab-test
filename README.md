# gitlab-test

## WHAT ARE THOSEEEEEE

This is a test repository for testing a specific GitLab workflow. For more information on the workflow, please visit the following article: https://about.gitlab.com/2014/09/29/gitlab-flow/

## BUT... WHY???

Gitlab simplifies the somewhat overcomplicated Github workflow just a tiny bit. It is a set of simple to understand commands that basically achieve the same result as a more complicated workflow. Unless you have a bunch of complicated branches or you're using commands and features such as upstream (or hacking the school Wifi), the GitLab workflow should suffice 99% of the time. To view the images of each workflow, go to the images folder.
